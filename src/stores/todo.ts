import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

interface Todo {
  id: number,
  text: string,
  isDone: boolean
}

export const useTodoStore = defineStore('todo', () => {
  const todos = ref<Todo[]>([])
  function add(payload: Todo) {
    todos.value.push(payload)
  }

  return { todos, add }
})
